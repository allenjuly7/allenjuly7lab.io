---
title: Assignment 2 - Qt及自研多线程框架
date: 2021-06-27 02:16:38
tags: [Qt, threads, framework]
---

本练习在 https://gitlab.com/seu228/framework-starter-pack 的基础上进行。在开始之前，先**fork该仓库**到自己的账号下，再进行开发。每个问题完成后**打一个tag**以便于后续查看。

关于Git操作相关问题可参考：

- https://git-scm.com/book/en/v2
- https://www.bootcss.com/p/git-guide/
- https://www.liaoxuefeng.com/wiki/896043488029600



## Q1 环境配置

将上述代码下载至本地计算机，并按照说明进行环境的配置。

- 若选择使用Clion进行开发，载入项目根目录的`CMakeLists.txt`文件即可（可能需要进行一定修改）
- 若选择使用VS或Qt Creator进行开发，需要自行新建工程，将代码文件添加到工程中，并配置OpenCV、spdlog等第三方库。
- Visual Studio需搭配[Qt Visual Studio Tools](https://marketplace.visualstudio.com/items?itemName=TheQtCompany.QtVisualStudioTools-19123)以较为方便地进行基于Qt的项目开发。

*建议对三种IDE都进行尝试以熟悉不同IDE下的环境配置操作。*

完成环境配置后即可编译、启动该程序。请选择一段mp4格式的视频以测试程序功能是否正常。



## Q2 新建模块

创建一个`ImageProcessHandler`模块，任选一种图像处理算法（滤波、模糊、检测等）对`CaptureHandler`采集到的图像进行处理后再进行显示。添加新模块后的系统示意图为


![system-overflow-with-process.jpeg](https://i.loli.net/2021/06/27/HGkp4S5hMdxlEZR.jpg)

Hints:

- 新增`HandlerType`、`SignalType`，理清该模块发出何种信号，监听何种信号；
- 理清数据的流动，该模块要处理的图像数据从何处取得，处理好的图像存到何处；
- 考虑新增模块对系统其它部分的影响，如`Repository`是否需要`allocate`新的空间？`DisplayHandler`监听的信号类型是否需要改变？



## Q3 框架改进

- 将`Dispatcher`类改为单例模式；
- 将`Repository`类改为单例模式；
- 修改代码中与上述变更相关的部分，使程序正常编译、运行。



## Q4 如何在此基础上同时播放两个视频文件？

编码前思考下列问题：

- 是否所有模块都需要两个实例？哪些模块可以两者共用一个实例？可以共用的原因或限制因素是什么？
- 两个视频同时播放时数据如何组织？共用实例的模块如何区分应该处理哪一路图像？处理模块如何取到某一路的图像数据，又如何将处理结果写入正确的地方？



*可参考 https://gitlab.com/seu228/detect-SH 项目的实现方式。*

