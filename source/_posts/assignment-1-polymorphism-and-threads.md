---
title: Assignment 1 - 多态及线程
date: 2021-05-19 00:04:44
tags: [polymorphism, threads]
---

## Q1 多态

创建三个类`Car`、`Tesla`、`Porsche`和一个函数`manufacture`，其中函数`manufacture`的签名为

```c++
Car *manufacture(const std::string& brand)
```

预期的调用方式为

```c++
Car *car1 = manufacture("Tesla");         // return pointer to Tesla object
car1->start();                            // print "Safe driving!"

Car *car2 = manufacture("Porsche");       // return pointer to Porsche object
car2->start();                            // print "Nice car!"

Car *car3 = manufacture("fck");           // return pointer to Car object for any other strings
car3->start();                            // print "Choose a brand!"
```

*尝试使用[智能指针](https://www.internalpointers.com/post/beginner-s-look-smart-pointers-modern-c)消除上述代码中可能存在的内存泄露问题。*

## Q2 线程

创建两个线程，使其交替按顺序打印出1至100的自然数，即

```c++
1               // thread A
2               // thread B
...
100             // thread B
```

线程中在输出数字后可进行适当延时以便于观察和调试。

线程的创建和管理请使用C++11中引入的`std::thread`。https://en.cppreference.com/w/cpp/header/thread
